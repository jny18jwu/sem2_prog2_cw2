#include "MovieDatabase.h"
#include <iostream>
#include <algorithm>
#include <cstring>

MovieDatabase::MovieDatabase() {}

void MovieDatabase::readFile(std::string fileName) {
    std::string line;
    std::ifstream file (fileName);
    if(file.is_open()) {
        while (getline (file,line)) {
            Movie m(line);
            movies.push_back(m);
        }
        file.close();
    }
    else std::cout << "Unable to open file";
}

void MovieDatabase::printMovies() {
       for(int i = 0; i<movies.size(); i++) {
           movies.at(i).print();
       }
}

std::vector<Movie> MovieDatabase::sortByAsccending(bool sortAscending) {
    std::sort(movies.begin(), movies.end(), Movie::compareYear);
    if (sortAscending) {
        return movies;
    }
    std::reverse(movies.begin(),movies.end());
    return movies;

}
Movie MovieDatabase::getMovieByGenre(std::string genre, int element) {
    std::vector<Movie> genreMovies;

    for (Movie m: movies) {

        if (m.getGenre().find(genre) != std::string::npos) {
            genreMovies.push_back(m);
        }
    }
    std::sort(genreMovies.begin(), genreMovies.end(), Movie::compareDuration);

    return genreMovies.at(element-1);
}

Movie MovieDatabase::getMovieByCertificate(std::string certificate, int element) {
    std::vector<Movie> certificateMovie;

    sortByAsccending(false);

    for (Movie m:movies) {
        if (m.getCertificate().find(certificate) != std::string::npos) {
            certificateMovie.push_back(m);
        }
    }

   return certificateMovie.at(element-1);
}

Movie MovieDatabase::getLongestMovieName() {
    int i = 0;
    int index = 0;
    int maxLength = 0;
    for (Movie m:movies) {
        int titleLength = m.getTitle().size();
        if (titleLength > maxLength) {
            maxLength = titleLength;
            index = i;
        }
        i++;
    }

    return movies.at(index);
}

/*1. Read in the database from the file films.txt, using the relave path “films.txt”,
provided via BlackBoard (when using CLion, the program will expect to find the file
        in the cmake-build-debug directory). This is necessary to ensure that the program
runs correctly using PASS.
2. Display the enre collecon of movies, arranged in chronological order. The movies
must be displayed in the same format in which they appear in films.txt.
3. Display the third longest Film-Noir.
4. Display the eighth most recent UNRATED film.
5. Display the film with the longest tle.*/

//http://www.cplusplus.com/doc/tutorial/files/
//https://www.geeksforgeeks.org/how-to-sort-an-array-of-dates-in-cc/
//http://www.cplusplus.com/forum/beginner/233668/