#include <iostream>
#include "Movie.h"
#include "MovieDatabase.h"

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MAIN  // in only one cpp file
//#include <boost/test/unit_test.hpp>
using namespace std;

int main() {
    Movie m("The Good, the Bad and the Ugly",1966,"APPROVED","Western",161,0);
    m.print();
    std::cout << "Certificate: " << m.getCertificate() << std::endl;
    std::cout << "Title: " << m.getTitle() << std::endl;
    std::cout << "Genre: " << m.getGenre() << std::endl;
    std::cout << "Duration: " << m.getDuration() << std::endl;
    std::cout << "Year: " << m.getYear() << std::endl;
    std::cout << "Rating: " << m.getRating() << std::endl;

    std::string str;
    str = R"("Indiana Jones and the Last Crusade",1989,"PG-13","Action/Adventure/Fantasy",127,0)";
    Movie mov(str);
    mov.print();
    std::cout << "Certificate: " << mov.getCertificate() << std::endl;
    std::cout << "Title: " << mov.getTitle() << std::endl;
    std::cout << "Genre: " << mov.getGenre() << std::endl;
    std::cout << "Duration: " << mov.getDuration() << std::endl;
    std::cout << "Year: " << mov.getYear() << std::endl;
    std::cout << "Rating: " << mov.getRating() << std::endl;

    MovieDatabase md;
    md.readFile("file.txt");
    //md.printMovies();

    std::cout << "\nDatabase query 1:" << std::endl;
    md.sortByAsccending(false);
    md.printMovies();

    std::cout << "\nDatabase query 2:" << std::endl;
    md.getMovieByGenre("Film-Noir",3).print();
    std::cout << "\nDatabase query 3:" << std::endl;
    md.getMovieByCertificate("UNRATED",8).print();
    std::cout << "\nDatabase query 4:" << std::endl;
    md.getLongestMovieName().print();
    return 0;
}
