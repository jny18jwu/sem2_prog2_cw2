#include <vector>
#include <string>
#include <fstream>
#include "Movie.h"


#ifndef MOVIEDATABASE_MOVIEDATABASE_H
#define MOVIEDATABASE_MOVIEDATABASE_H

class MovieDatabase {
private:
    std::vector<Movie> movies;

public:
    MovieDatabase();
    void readFile(std::string fileName);
    void printMovies();
    std::vector<Movie> sortByAsccending(bool sortAscending);
    Movie getMovieByGenre(std::string genre, int element);
    Movie getMovieByCertificate(std::string certificate, int element);
    Movie getLongestMovieName();
};

#endif //MOVIEDATABASE_MOVIEDATABASE_H

//https://en.cppreference.com/w/cpp/container/vector