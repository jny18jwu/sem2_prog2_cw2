#ifndef MOVIEDATABASE_MOVIE_H
#define MOVIEDATABASE_MOVIE_H

#include <iostream>
#include <string>

class Movie {
private:
    std::string title;
    int year;
    std::string certificate;
    std::string genre;
    int duration;
    double rating;

public:
    Movie(std::string title, int year, std::string certificate, std::string genre, int duration, double rating);
    Movie(std::string line);
    void print();

    std::string getTitle();
    int getYear();
    std::string getCertificate();
    std::string getGenre();
    int getDuration();
    double getRating();
    static bool compareYear(Movie m1, Movie m2);
    static bool compareDuration(Movie m1, Movie m2);
};


#endif //MOVIEDATABASE_MOVIE_H
